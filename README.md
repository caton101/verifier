# Verifier

This program checks the sha256sum of all files below the parent folder. It makes checking for curruption quick and painless.

## Install

1. Install Python 3.x

    For MacOS: [download from the official site](https://www.python.org/downloads/)

    For Linux/BSD: see your distro's documentation

    For other systems: [try the alternate downloads](https://www.python.org/download/other/) or [build from source](https://www.python.org/downloads/source/)

2. Clone the git repo

    example: `git clone https://gitlab.com/caton101/verifier`

3. Enter the program directory

    example: `cd verifier`

4. Run the installer script

    example: `./install`

5. Test the program using `verifier --version`. The program is installed if you see the following output:

    ```shell
    Verifier by Cameron Himes
    Version A.0.1
    ```

NOTE: The version line might look different.

## How To Use Verifier

1. Ensure that Verifier is installed by running `verifier --version`. The program is installed if you see the following output:

    ```shell
    Verifier by Cameron Himes
    Version A.0.1
    ```

NOTE: The version line might look different.

2. Go to the parent directory of the files you want to copy
3. Run `verifier --generate`
4. Copy files to the new location (server, new drive, ect.)
5. Run `verifier --verify`
6. Check the output for corrupted files

    You will see a list of bad files at the bottom of the output:

    ```shell
    File verification complete.
    The following files failed:
            --> "./README.md"
    No files were missing from drive.
    All files had verification data.
    No directories were missing from drive.
    All directories had verification data.
    ```

    You will see this at the bottom of the output of all files are correct:

    ```shell
    File verification complete.
    No files failed.
    No files were missing from drive.
    All files had verification data.
    No directories were missing from drive.
    All directories had verification data.
    ```

7. If you wish to clean up your files, you can delete verification metadata by running `verifier --delete`

## Command Usage

Verifier can be run without any arguements:

`verifier`

Alternatively, you can pass an option to the program:

`verifier [option]`

A list of options can be seen below or by using `verifier --help`

| Option     | Description                    |
| :--------- | :----------------------------- |
| --generate | generate verification metadata |
| --verify   | check files for corruption     |
| --delete   | delete verification metadata   |
| --help     | show help and exit             |
| --version  | show version and exit          |
