#   Verifier
#   Copyright (C) 2019  Cameron Himes

#   This program is free software: you can redistribute it and/or modify
#   it under the terms of the GNU General Public License as published by
#   the Free Software Foundation, either version 3 of the License, or
#   (at your option) any later version.

#   This program is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#   GNU General Public License for more details.

#   You should have received a copy of the GNU General Public License
#   along with this program.  If not, see <http://www.gnu.org/licenses/>.

import json

# example dict
#
# example showing what the dict being processed should look like
example = {
    "directories" : ["Desktop", "Documents", "Music", "Videos", "Pictures"],
    "files" : {
        ".bashrc" : "skjhgfdskjghdslkjghldskhgsdlghsldgdsg",
        ".nanorc" : "ryriefkjdshfslkfhlksahfldsakfhslkfhsa"
    },
    "isParent" : True
}

# writeData function
#
# converts a dict to json and then writes it to a file
# args:
#       dictionaryData: dict,
#       path: str, a system path specifying where the file should be stored
def writeData(directoryData, path="."):
    # check dict for errors
    if not checkSanity(directoryData):
        raise TypeError("[ERROR] Passed invalid data to file writer.")
    # convert dict to a json formatted string
    js = json.dumps(directoryData,indent=4)
    # open file for writing
    f = open(path + "/.verifydata","w")
    # write json string to file
    f.write(js)
    # close file
    f.close()

# readData function
#
# reads json data from a file and converts it to a dict
# args:
#       path: str, a system path specifying where the file was stored
# return:
#       a dict formed from json data stored in a file
def readData(path="."):
    # open file for reading
    f = open(path + "/.verifydata","r")
    # get string from file
    text = f.read()
    # close file
    f.close()
    # convert json formatted string to dict
    data = json.loads(text)
    # check that the dict is correct
    if not checkSanity(data):
        raise TypeError("[ERROR] Read invalid data from file.")
    # return the dict
    return data

# __debugSilent__ function
#
# a helper function for not printing debug output
# args:
#       text: str, text to print
def __debugSilent__(text):
    # this helper does nothing
    # tell python to do nothing
    pass

# __debugWrite__ function
#
# a helper function for printing debug output
# args:
#       text: str, text to print
def __debugWrite__(text):
    # this helper prints the passed text
    # print text
    print(text)

# checkSanity function
#
# checks dict information for proper storage formatting
# args:
#       directoryData: dict, the data to be checked for errors
#       showDebug: bool, true if debugging information is wanted
# return:
#       a bool that is true if the data is properly formatted
def checkSanity(directoryData, showDebug=False):
    # store the address of a helper function depending if the user wants debug info
    if showDebug:
        # use the address of the noisy helper function
        write = __debugWrite__
    else:
        # use the address of the quiet helper function
        write = __debugSilent__
    # check data type of the given object
    if type(directoryData) is dict:
        write("(01/10) Object is type dict.")
        # check for the directories key
        if "directories" in directoryData:
            write("(02/10) The dict contains a \"directory\" field.")
            # check data type of the directories value
            if type(directoryData["directories"]) == list:
                write("(03/10) \"directory\" field is type list.")
                # ensure all values in the directories list are strings
                for f in directoryData["directories"]:
                    if not type(f) == str:
                        # the list contains something besides strings
                        write("(04/10) \"directories\" list contains something that is not type str.")
                        return False
                write("(04/10) \"directory\" list contains only type str.")
            else:
                # the directories value is not a list
                write("(03/10) \"directory\" field is not type list.")
                return False
        else:
            # the dict is missing the directories key
            write("(02/10) The dict does not contain a \"directories\" field.")
            return False
        # check for the files key
        if "files" in directoryData:
            write("(05/10) The dict contains a \"files\" field.")
            # check data type of the files value
            if type(directoryData["files"]) == dict:
                write("(06/10) \"files\" field is type dict.")
                # ensure all keys and values in the files dict are strings
                for f in directoryData["files"]:
                    if not type(f) == str:
                        # found a dict key that is not a string
                        write("(07/10) \"files\" dict must contain keys as type str.")
                        return False
                    if not type(directoryData["files"][f]) == str:
                        # found a dict value that is not a string
                        write("(07/10) \"files\" dict must store strings inside keys.")
                        return False
                write("(07/10) \"files\" dict keys and values are type str.")
            else:
                # the files value is not a dict
                write("(06/10) \"files\" field is not type dict.")
                return False
        else:
            # the dict is missing the files key
            write("(05/10) The dict does not contain a \"files\" field.")
            return False
        # check for the isParent key
        if "isParent" in directoryData:
            write("(08/10) The dict contains a \"isParent\" field.")
            # check the data type of the isParent value
            if type(directoryData["isParent"]) == bool:
                write("(09/10) \"isParent\" field is type bool.")
            else:
                # the value is not a bool
                write("(09/10) \"isParent\" is not type bool.")
                return False
        else:
            # the dict is missing the isParent key
            write("(08/10) The dict does not contain a \"isParent\" field.")
            return False
        # if the program got here, everything is correct
        write("(10/10) All checks passed.")
        return True
    else:
        # the object given is not a dict
        write("(01/10) Object is not type dict.")
        return False

# __test__ function
#
# checks if the various functions inside this file are correct
def __test__():
    # display the example data
    print("=== PRINT TEST DICT ===")
    print(example)
    # check the sanity of the example data
    print("=== CHECK TEST DICT ===")
    print(checkSanity(example,showDebug=True))
    # save example data to a file
    print("=== SAVE TEST DICT ===")
    writeData(example)
    # load example data from a file
    print("=== LOAD SAVED DICT ===")
    x = readData()
    # display data read from a file
    print("=== PRINT LOADED DICT ===")
    print(x)
    # check sanity of data read from a file
    print("=== CHECK LOADED DICT ===")
    print(checkSanity(x))
    # testing is over
    print("=== DONE ===")