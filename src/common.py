#   Verifier
#   Copyright (C) 2019  Cameron Himes

#   This program is free software: you can redistribute it and/or modify
#   it under the terms of the GNU General Public License as published by
#   the Free Software Foundation, either version 3 of the License, or
#   (at your option) any later version.

#   This program is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#   GNU General Public License for more details.

#   You should have received a copy of the GNU General Public License
#   along with this program.  If not, see <http://www.gnu.org/licenses/>.

import hashlib
import os
import fileio

# FileData class
#
# stores file information for processing
# args:
#       name: str, the name of the file
#       checksum: str, the sha256-sum of the file (optional, but must be added before saving)
class FileData(object):
    name = None
    checksum = None
    def __init__(self, name, checksum=""):
        self.name = name
        self.checksum = checksum

# convertToStore function
#
# convert arrays of file objects and directories to a single dictionary for storage
# args:
#       isParent: bool, true if the file is being placed in the root directory
#       directories: list of str, a list of strings containing directory names
#       files: list of FileData, a list of file objects containing a file name and checksum
# return:
#       a dict in proper formatting to be saved in a configuration file
def convertToStore(isParent, directories, files):
    # make dict template
    store = {
        "directories" : directories,
        "files" : {},
        "isParent" : isParent
    }
    # add files to dict
    for fileObj in files:
        store["files"][fileObj.name] = fileObj.checksum
    # return the final dict
    return store

# convertFromStore function
#
# convert a dictonary from storage to an array of file objects and directories
# args:
#       storedDict: dict, a dict read from a configuration file
# returns:
#       isParent: bool, true if the file was placed in the root directory
#       directories: list of str, a list of strings containing directory names
#       files: list of FileData, a list of file objects containing a file name and checksum
def convertFromStorage(storedDict):
    # pull values from dict
    directories = storedDict["directories"]
    isParent = storedDict["isParent"]
    filesDict = storedDict["files"]
    # prepare list for FileData
    files = []
    # iterate over files in dict
    for key in filesDict:
        # convert files in dict to objects and add them to the list
        files.append(FileData(key,storedDict["files"][key]))
    # return processed data gathered from the dict
    return isParent, directories, files

# calcChecksum function
#
# calculate the sha256sum of a file
# args:
#       filePath: str, path of file to scan
# return:
#       a str containing the sha256sum of the given file
def calcChecksum(filePath):
    # set mode
    x = hashlib.sha256()
    # open file
    f = open(filePath,"rb")
    while True:
        # read chunk from file
        data = f.read(1000000000)
        # determine if EOF
        if data == b'':
            # stop scanning since EOF was hit
            break
        # add chunk to hashlib
        x.update(data)
    # get hash of file
    checksum = x.hexdigest()
    # close file
    f.close()
    # return checksum
    return checksum

# getFolderData function
#
# gather directory listing of a given path
# args:
#       filePath: str, path of file to scan
# returns:
#       files: list of str, list of all files in the directory
#       directories: list of str, list of all subdirectories in the directory
def getFolderData(filePath="."):
    # make lists for files and subdirectories
    files = []
    directories = []
    # iterate over directory listing
    for item in os.listdir(filePath):
        # get path to each item
        itemPath = filePath + "/" + item
        # do not process symlinks
        if os.path.islink(itemPath):
            # ignore this iteration
            continue
        # check if a listing is a subdirectory
        if os.path.isdir(itemPath):
            # add to subdirectory list
            directories.append(item)
        # check if listing is a file
        elif os.path.isfile(itemPath):
            # add to files list
            files.append(item)
        else:
            # inform the user that this program is not designed to handle the listing
            print("Found directory item that is not a file or a directory.")
    # return final lists
    return files, directories

# testForMeta function
#
# checks a directory path for verification data
# args:
#       filePath: str, path of directory to scan
# return:
#       a bool that is true if verification data is found
def testForMeta(filePath):
    # get a list of files
    files, _ = getFolderData(filePath)
    # search list for verification data
    if ".verifydata" in files:
        # found data, return true
        return True
    # did not find data, return false
    return False

# testIfParent function
#
# reads the verification data of a path to determine if a path is a parent
# args:
#       filePath: str, path of directory to scan
# return:
#       a bool that is true if the current path is the parent
def testIfParent(filePath):
    # get the contents of the path
    rawMeta = fileio.readData(filePath)
    # fetch the parent flag from stored data
    isParent, _, _ = convertFromStorage(rawMeta)
    # check the stored parent flag
    if isParent:
        # directory is parent, return true
        return True
    else:
        # directory is not the parent, return false
        return False

# findParent function
#
# search up the file path until the parent directory is found
# args:
#       filePath: str, path of directory to scan
# return:
#       a string containing the path of the parent directory
def findParent(filePath):
    # make a temp path to store parent path
    testPath = filePath
    # keep checking if the temp path is the parent
    while not testIfParent(testPath):
        # the temp path is not the parent, go up a directory
        testPath += "/.."
    # found the parent path, return it
    return testPath

# changeToParent function
#
# change the current directory to the parent directory
def changeToParent():
    # check if the current directory has metadata
    if testForMeta("."):
        # check if the current path is the parent
        if not testIfParent("."):
            # find the parent path
            path = findParent(".")
            # set Python to use the parent path
            os.chdir(path)

# deleteOldMeta function
#
# delete all metadata files from a given directory path
def deleteOldMeta(dirPath="."):
    # check for metadata in the given path
    if not testForMeta(dirPath):
        # the current directory does not contain metadata, do not search children for metadata
        return
    # found metadata, recursively search directories for metadata to delete
    __seekAndDelete__(dirPath)

# __seekAndDelete__ function
#
# a helper function for traveling down a directory tree and deleting metadata
def __seekAndDelete__(dirPath):
    # iterate over a directory listing
    for thing in os.listdir(dirPath):
        # make a path containing the current directory and the found item
        target = dirPath + "/" + thing
        # test of the made path is a directory
        if os.path.isdir(target):
            # travel into found directory
            __seekAndDelete__(target)
    # try to delete metadata
    try:
        # attempt to delete verification data from current directory
        os.remove(dirPath + "/.verifydata")
        # inform user that metadata was deleted
        print("Deleted verification file at %s/.verifydata" % (dirPath))
    except FileNotFoundError:
        # metadata did not exist, just ignore this error
        pass