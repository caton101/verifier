#   Verifier
#   Copyright (C) 2019  Cameron Himes

#   This program is free software: you can redistribute it and/or modify
#   it under the terms of the GNU General Public License as published by
#   the Free Software Foundation, either version 3 of the License, or
#   (at your option) any later version.

#   This program is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#   GNU General Public License for more details.

#   You should have received a copy of the GNU General Public License
#   along with this program.  If not, see <http://www.gnu.org/licenses/>.

import generator
import verifier
import common
import sys

# callGenerator function
#
# call the metadata generator on the current directory
def callGenerator():
    # start the generator
    generator.startGenerator(".")

# callVerifier function
#
# call the file verifier on the current directory
def callVerifier():
    # start the verifier
    verifier.startVerifier(".")

# callDelete function
#
# recursively delete all metadata from the directory tree
def callDelete():
    # start the file deletion function
    common.deleteOldMeta(".")

# showHelp function
#
# show a list of all program options
def showHelp():
    # show program options to user
    print(
        "Usage: verifier [option]\n" +
        "All options:\n" +
        "    --generate  : generate verification metadata\n" +
        "    --verify    : check files for corruption\n" +
        "    --delete    : delete verification metadata\n" +
        "    --help      : show help and exit\n" +
        "    --license   : show program license\n" +
        "    --license-w : show license warranty\n" +
        "    --license-c : show license conditions\n" +
        "    --version   : show version and exit"
    )

# showVersion function
#
# show the program version
def showVersion():
    # show version information for user
    print("Verifier by Cameron Himes")
    print("Version A.3.1")

# showGPL3 function
#
# show the GPL3 summary
def showGPL3():
    # show GPL2 summary to user
    print("""Verifier  Copyright (C) 2019  Cameron Himes
This program comes with ABSOLUTELY NO WARRANTY; for details use option `--license-w'.
This is free software, and you are welcome to redistribute it
under certain conditions; use option `--license-c' for details.""")

# showGPL3Conditions function
#
# show the GPL3 conditions
def showGPL3Conditions():
    # show GPL2 conditions to user
    print("""This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.""")

# showGPL3Warranty function
#
# show the GPL3 warranty
def showGPL3Warranty():
    # show GPL2 warranty to user
    print("""This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.""")

# menu function
#
# prompt the user for task
def menu():
    # write GPL3 summary since this is legally an interactive mode
    showGPL3()
    # keep prompting the user until the program is closed
    while True:
        # show list of possible actions to user
        print(
            "What would you like to do?\n" +
            "    1) Generate verification files.\n" +
            "    2) Check files for corruption.\n" +
            "    3) Remove verification files.\n" +
            "    4) Show help.\n" +
            "    5) Show version.\n" +
            "    6) Show license.\n" +
            "    7) Show license warranty.\n" +
            "    8) Show license conditions.\n" +
            "    9) Exit program."
        )
        # get answer from user
        ans = input("Enter choice: ")
        if ans == "1":
            # generate metadata
            callGenerator()
        elif ans == "2":
            # verify files
            callVerifier()
        elif ans == "3":
            # delete metadata
            callDelete()
        elif ans == "4":
            # show help list
            showHelp()
        elif ans == "5":
            # show version information
            showVersion()
        elif ans == "6":
            # show GPL3 summary
            showGPL3()
        elif ans == "7":
            # show GPL3 warranty
            showGPL3Warranty()
        elif ans == "8":
            # show GPL3 conditions
            showGPL3Conditions()
        elif ans == "9":
            # exit program
            exit()

# parseArgs function
#
# parse arguement from user and perform specified task
# args:
#       args: list of str, arguments to parse
# NOTE:
#       This only processes the first arguement since the
#       shell script can only pass one arguement to Python.
# NOTE:
#       Arguement at index 0 is skipped since Python always
#       provides current working directory at index 0 and
#       this program does not need that information.
def parseArgs(args):
    for arg in args[1:]:
        if arg == "--generate":
            # generate metadata
            callGenerator()
        elif arg == "--verify":
            # verify files
            callVerifier()
        elif arg == "--delete":
            # delete metadata
            callDelete()
        elif arg == "--help":
            # show help list
            showHelp()
        elif arg == "--version":
            # show version information
            showVersion()
        elif arg == "--license":
            # show GPL3 summary
            showGPL3()
        elif arg == "--license-w":
            # show GPL3 warranty
            showGPL3Warranty()
        elif arg == "--license-c":
            # show GPL3 conditions
            showGPL3Conditions()
        elif arg == "--debug":
            # print current path
            print(sys.argv)
            import os
            print(os.path.abspath("."))
        else:
            # exit program
            print("Invalid option \"%s\". Please see --help for valid options." % (arg))
            exit()

# main function
#
# process input and run program
def main():
    # check for passed args
    if len(sys.argv) > 1:
        # run automatically
        parseArgs(sys.argv)
    else:
        # run interactively
        menu()

# call main function and start program
main()