#   Verifier
#   Copyright (C) 2019  Cameron Himes

#   This program is free software: you can redistribute it and/or modify
#   it under the terms of the GNU General Public License as published by
#   the Free Software Foundation, either version 3 of the License, or
#   (at your option) any later version.

#   This program is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#   GNU General Public License for more details.

#   You should have received a copy of the GNU General Public License
#   along with this program.  If not, see <http://www.gnu.org/licenses/>.

import common
import fileio

# generateFolder function
#
# recursively enter all directories and make verification data
# args:
#       currentPath: str, path of directory to scan
#       isParent: boolean, true if the current path is the parent directory
def generateFolder(currentPath, isParent=True):
    # check if directory has old verification data
    if common.testForMeta(currentPath):
        # check if the current path is the old parent directory
        if common.testIfParent(currentPath):
            # recursively delete old verification files from directory tree
            print("Removing old scan files...")
            common.deleteOldMeta(currentPath)
    # get a list of files and directories
    files, directories = common.getFolderData(currentPath)
    # recursively enter all directories
    for directory in directories:
        # call using the newly found directory and set isParent to false
        generateFolder(currentPath + "/" + directory, isParent=False)
    # inform user of the directory being scanned
    print("Scanning directory " + currentPath)
    # make an array for FileData objects
    scannedFileData = []
    # iterate over all found files
    for fileName in files:
        # get path to current file
        filePath = currentPath + "/" + fileName
        # inform user of the file being scanned
        print("Calculating checksum for " + filePath)
        # get the checksum of the file being scanned
        checksum = common.calcChecksum(filePath)
        # made a new FileData object and add it to the array
        scannedFileData.append(common.FileData(fileName,checksum))
    # convert directory data to the archive format
    storeDict = common.convertToStore(isParent, directories, scannedFileData)
    # write directory data to a file
    fileio.writeData(storeDict, currentPath)

# checkOldRun function
#
# test for verification data and ask for next operation
# args:
#       currentPath: str, path of directory to scan
# return:
#       a boolean that is true if new verification files are to be generated
def checkOldRun(currentPath):
    # check for old metadata
    if common.testForMeta(currentPath):
        # check if current path is the old parent
        if not common.testIfParent(currentPath):
            # keep prompting the user for the next task
            while True:
                # list all possible options
                print(
                    "This is not the parent folder.\n" +
                    "How would you like to continue?\n" +
                    "    1) Delete old scan files and continue from current directory.\n" +
                    "    2) Delete old scan files and continue from old parent directory.\n" +
                    "    3) Abort."
                )
                # get answer from user
                ans = input("Enter choice: ")
                if ans == "1":
                    # get old parent path
                    oldParent = common.findParent(currentPath)
                    # delete all files from old scan
                    common.deleteOldMeta(oldParent)
                    # user wants to perform scan, return true
                    return True
                elif ans == "2":
                    # change to old parent path
                    common.changeToParent()
                    # delete old scan files
                    common.deleteOldMeta(".")
                    # user wants to perform scan, return true
                    return True
                elif ans == "3":
                    # user does not want to perform scan, return false
                    return False
        # the current path is the old parent, scan as usual
        print("Current directory is the old parent.")
        return True
    # an old scan was not found, scan as usual
    print("No old verification files found.")
    return True

# startGenerator function
#
# main function to run the file generator
# args:
#       rootPath: str, path of directory to scan
def startGenerator(rootPath):
    # check for old scan files
    print("Checking for old verification files...")
    canContinue = checkOldRun(rootPath)
    # check if generator can run
    if canContinue:
        # generate verification files
        print("Generating verification files...")
        generateFolder(rootPath)
        print("File generation complete.")
    else:
        # do nothing
        print("File generation failed to start.")

# __test__ function
#
# test the generator by calling it on the current directory
def __test__():
    # start the generator
    startGenerator(".")