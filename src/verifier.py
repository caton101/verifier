#   Verifier
#   Copyright (C) 2019  Cameron Himes

#   This program is free software: you can redistribute it and/or modify
#   it under the terms of the GNU General Public License as published by
#   the Free Software Foundation, either version 3 of the License, or
#   (at your option) any later version.

#   This program is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#   GNU General Public License for more details.

#   You should have received a copy of the GNU General Public License
#   along with this program.  If not, see <http://www.gnu.org/licenses/>.

import common
import fileio

# crossCheckDirectory function
#
# check disk and metadata listings for differences
# args:
#       meta: list of str, list of directory names from the metadata file
#       os: list of str, list of directory names from the disk
# returns:
#       scanQueue: list of str, names of directories found in both the metadata and disk
#       notOnDisk: list of str, names of directories found in metadata, but not on disk
#       notInMeta: list of str, names of directories found on disk, but not in metadata
def crossCheckDirectory(meta, os):
    # make a queue of directories to check
    scanQueue = meta
    # make lists for errors
    notOnDisk = []
    notInMeta = []
    # check meta against disk
    for item in meta:
        if not item in os:
            # directory is not on disk anymore, remove it
            notOnDisk.append(item)
            scanQueue.remove(item)
    # check disk against meta
    for item in os:
        if not item in meta:
            # directory was never in meta, report the error
            notInMeta.append(item)
    # return results of crosscheck
    return scanQueue, notOnDisk, notInMeta

# crossCheckFiles function
#
# check disk and metadata listings for differences
# args:
#       meta: list of FileData, list of FileData objects from the metadata file
#       os: list of str, list of file names from the disk
# returns:
#       scanQueue: list of FileData, files found in both the metadata and disk
#       notOnDisk: list of str, names of files found in metadata, but not on disk
#       notInMeta: list of str, names of files found on disk, but not in metadata
def crossCheckFiles(meta, os):
    # make a queue of FileData to check
    scanQueue = meta
    # make lists for errors
    notOnDisk = []
    notInMeta = []
    # check meta against disk
    for fileObj in meta:
        if not fileObj.name in os:
            # file is not on disk anymore, remove it
            notOnDisk.append(fileObj.name)
            scanQueue.remove(fileObj)
    # check disk against meta
    for item in os:
        # do not check metadata files
        if item == ".verifydata":
            # skip this loop iteration
            continue
        # make flag for search
        found = False
        # try to find metadata for file on disk
        for fileObj in meta:
            # compare names
            if fileObj.name == item:
                # file on disk matched metadata
                found = True
                break
        if not found:
            # item on disk was not in metadata
            notInMeta.append(item)
    # return results of crosscheck
    return scanQueue, notOnDisk, notInMeta        

# annexPaths function
#
# combine files and the given path
# args:
#       path: str, file path
#       nameList: list of str, files to process
# returns:
#       list of paths to the listed files
def annexPaths(path, nameList):
    # make list for complete paths
    toReturn = []
    # iterate over all files
    for name in nameList:
        # add file to the path and append to final list
        toReturn.append(path + "/" + name)
    # return list of complete paths
    return toReturn

# verify function
#
# check disk and metadata listings for differences
# args:
#       currentPath: str, path of file to scan
# returns:
#       failedFiles: list of str, paths to files that failed the checksum comparison
#       filesNotOnDisk: list of str, paths to files that are in metadata, but not on disk
#       filesNotInMeta: list of str, paths to files that are on disk, but not in metadata
#       directoriesNotOnDisk: list of str, paths to directories that are in metadata, but not on disk
#       directoriesNotInMeta: list of str, paths to directories that are on disk, but not in metadata
def verify(currentPath):
    # check for verification file
    if not common.testForMeta(currentPath):
        return [],[currentPath + "/.verifydata"],[],[],[]
    # get contents from meta
    rawMeta = fileio.readData(currentPath)
    _, metaDirectories, metaFiles = common.convertFromStorage(rawMeta)
    # perform conversion
    metaFileNames = []
    for fileObject in metaFiles:
        metaFileNames.append(fileObject.name)
    # get directory listing from OS
    osFiles, osDirectories = common.getFolderData(currentPath)
    # cross-check
    fileQueue, filesNotOnDisk, filesNotInMeta = crossCheckFiles(metaFiles, osFiles)
    directoryQueue, directoriesNotOnDisk, directoriesNotInMeta = crossCheckDirectory(metaDirectories, osDirectories)
    # get full paths
    filesNotOnDisk = annexPaths(currentPath, filesNotOnDisk)
    filesNotInMeta = annexPaths(currentPath, filesNotInMeta)
    directoriesNotOnDisk = annexPaths(currentPath, directoriesNotOnDisk)
    directoriesNotInMeta = annexPaths(currentPath, directoriesNotInMeta)
    # make container for failed files
    failedFiles = []
    # enter lower directory
    for directory in directoryQueue:
        # run recursively
        newFailedFiles, newFilesNotOnDisk, newFilesNotInMeta, newDirectoriesNotOnDisk, newDirectoriesNotInMeta = verify(currentPath + "/" + directory)
        failedFiles.extend(newFailedFiles)
        filesNotOnDisk.extend(newFilesNotOnDisk)
        filesNotInMeta.extend(newFilesNotInMeta)
        directoriesNotOnDisk.extend(newDirectoriesNotOnDisk)
        directoriesNotInMeta.extend(newDirectoriesNotInMeta)
    # check files in queue
    for fileObject in fileQueue:
        print("Checking file \"%s\"" % (currentPath + "/" + fileObject.name))
        checksum = common.calcChecksum(currentPath + "/" + fileObject.name)
        if not checksum == fileObject.checksum:
            failedFiles.append(currentPath + "/" + fileObject.name)
    # return errors
    return failedFiles, filesNotOnDisk, filesNotInMeta, directoriesNotOnDisk, directoriesNotInMeta

# checkPreconditions function
#
# test for verification data and ask for next operation
# args:
#       currentPath: str, path of directory to scan
# return:
#       a boolean that is true if files are to be checked
def checkPreconditions(currentPath):
    # check for metadata
    if common.testForMeta(currentPath):
        print("Verification data found.")
        # check if current path is the parent
        if not common.testIfParent(currentPath):
            # keep prompting the user for the next task
            while True:
                # list all possible options
                print(
                    "This is not the parent folder.\n" +
                    "How would you like to continue?\n" +
                    "    1) Scan from current directory.\n" +
                    "    2) Scan from the parent directory.\n" +
                    "    3) Abort."
                )
                # get answer from user
                ans = input("Enter choice: ")
                if ans == "1":
                    # scan from current path, return true
                    return True
                elif ans == "2":
                    # change to the parent directory
                    common.changeToParent()
                    # user wants to perform scan, return true
                    return True
                elif ans == "3":
                    # user does not want to perform scan, return false
                    return False
        else:
            # the current path is the parent, scan as usual
            print("Current directory is the parent.")
            return True
    else:
        # metadata was not found, do not scan
        print("Current directory does not contain verification data.")
        return False

# startVerifier function
#
# main function to run the file verifier
# args:
#       rootPath: str, path of directory to scan
def startVerifier(rootPath):
    # check for metadata
    print("Checking verification data...")
    cancontinue = checkPreconditions(rootPath)
    # check if verifier can run
    if cancontinue:
        # check files
        print("Checking files for corruption...")
        failedFiles, filesNotOnDisk, filesNotInMeta, directoriesNotOnDisk, directoriesNotInMeta = verify(rootPath)
        print("File verification complete.")
        # report any errors
        if len(failedFiles) > 0:
            print("The following files failed:")
            for x in failedFiles:
                print("\t--> \"%s\"" % (x))
        else:
            print("No files failed.")
        if len(filesNotOnDisk) > 0:
            print("The following files were missing from drive:")
            for x in filesNotOnDisk:
                print("\t--> \"%s\"" % (x))
        else:
            print("No files were missing from drive.")
        if len(filesNotInMeta) > 0:
            print("The following files had no verification data:")
            for x in filesNotInMeta:
                print("\t--> \"%s\"" % (x))
        else:
            print("All files had verification data.")
        if len(directoriesNotOnDisk) > 0:
            print("The following directories were missing from drive:")
            for x in directoriesNotOnDisk:
                print("\t--> \"%s\"" % (x))
        else:
            print("No directories were missing from drive.")
        if len(directoriesNotInMeta) > 0:
            print("The following directories had no verification data:")
            for x in directoriesNotInMeta:
                print("\t--> \"%s\"" % (x))
        else:
            print("All directories had verification data.")
    else:
        # do nothing
        print("File verification failed to start.")

# __test__ function
#
# test the verifier by calling it on the current directory
def __test__():
    # start the verifier
    startVerifier(".")